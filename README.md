# Application Blueprint for Ghost

## What is Ghost?

From [ghost.org](https://ghost.org):

> Ghost is a powerful app for new-media creators to publish, share, and grow a business around their content.

This application blueprint uses the [official ghost](https://hub.docker.com/_/ghost) Docker image.

## Filling out a deployment blueprint
For the general steps to deploy an application with Unfurl Cloud, see our [Getting Started guides](https://unfurl.cloud/help#guides).

### Details
To simplify the deployment process, we have set default values for several required environment variables and hidden them from the UI. The remaining environment variables are exposed as the following “Detail” fields, which you must fill in after selecting a deployment:

| Input Name                    | Default Value    |
|-------------------------------|------------------|
| URL                           | `<subdomain>.<your-zone>`|
| Mail From                     | (none)           |
| Mail Options Host             | (none)           |
| Mail Options Auth User        | (none)           |
| Mail Options Auth Pass        | (none)           |

Below is a list of the hidden default environment variables and their values. As explained above, you will not encounter these environment variables as you fill in the deployment blueprint:

| Input Name                    | Default Value    |
|-------------------------------|------------------|
| Database Client               | `sqlite3`        |
| Database Connection Filename  | `content/data/ghost.db`|
| Database useNullAsDefault     | `true`           |
| Mail Options Port             | `587`            |
| Mail Transport                | `SMTP`           |

In the near future we will update this blueprint to optionally use a user-configured MariaDB/MySQL resource.

### Components
Each application blueprint includes **components**. These are the required and optional resources for the application. In most cases, there is more than one way to fulfill a component requirement. After you select a deployment blueprint, you will be prompted to fulfill the component requirements and configure the deployment to your needs. Common components include Compute, DNS, Database, and Mail Server:

#### Compute

Ghost requires the following compute resources:

- At least 1 CPU
- At least 1024MB of RAM
- At least 16GB of hard disk space

#### DNS

A DNS provider must be specified for the deployed site to be accessible via a domain name.

Supported DNS providers are:

- Google Cloud DNS
- DigitalOcean DNS
- AWS Route53
- Azure DNS

All providers require a domain name to use. ***Note:** the domain must be registered to that service.*

The `Subdomain` input above will be used to register a new subdomain under the given domain. For example, given the subdomain `blog` and domain zone `mysite.com`, the site will be accessible at `blog.mysite.com`.

#### Mail Server

Ghost requires a mail server to send out emails.

Two mail providers are supported, with their needed inputs detailed below:

<!-- use <br> for multi-line tables, GFM does not support multi-line tables -->

**SendGrid:**

| Input   | Description                                                                                    |
|---------|------------------------------------------------------------------------------------------------|
| API Key | This can be created at [SendGrid account settings](https://app.sendgrid.com/settings/api_keys) |

**Generic SMTP Server:**

| Input     | Description                                                                     |
|-----------|---------------------------------------------------------------------------------|
| SMTP Host | Name of the mail server, e.g. `smtp.someprovider.com`                           |
| User Name | Usually the email to send as. Double check the documentation for your provider! |
| Secret    | The password or login token to authenticate with.                               |
| Protocol  | Either `ssl` or `tls`, probably `tls`.                                          |

**Gmail:**

Use Generic SMTP Server with the following inputs:

| Input     | Value                                       |
|-----------|---------------------------------------------|
| SMTP Host | `smtp.gmail.com`                            |
| User Name | Gmail username (email without `@gmail.com`) |
| Secret    | Gmail password                              |
| Protocol  | `tls`                                       |
